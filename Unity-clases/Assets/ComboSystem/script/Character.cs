﻿/////////////////////////////////////////////////////////////////////////////////
////
////              Author: Guillermo Blanca
////              
////
////////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public Animator AnimatorRef { get; private set; }
    public MovementComponent movementComponent;
    protected AnimatorOverrideController animatorOverrideController;
    protected LifeController lifeController;
    public ComboManager comboManager;
    public Weapon currentWeapon;

    private bool hasCombo;
    public bool isAttacking { get; private set; }
    [SerializeField] AnimationClip basicAttackAnim;

    private int NormalAttackHasCode = Animator.StringToHash("NormalAttack");
    private bool isDeath;
    // Start is called before the first frame update
    protected virtual void Start()
    {
        AnimatorRef = GetComponent<Animator>();
        comboManager = GetComponent<ComboManager>();
        movementComponent = GetComponent<MovementComponent>();

        comboManager.OnExecuteCombo += (OnComboExecute);
        comboManager.OnEndCombo += (OnComoboStop);
        comboManager.Owner = this;

        animatorOverrideController = new AnimatorOverrideController(AnimatorRef.runtimeAnimatorController);
        AnimatorRef.runtimeAnimatorController = animatorOverrideController;

        Cursor.lockState = CursorLockMode.Locked;

        lifeController = GetComponent<LifeController>();
        lifeController.OnDeath.AddListener(Death);
    }
    void OnDestroy()
    {
        comboManager.OnExecuteCombo -= OnComboExecute;
    }
    protected virtual void Update()
    {
        
        if (hasCombo || isAttacking || isDeath)
            return;
     //  movementComponent.InputMagnitude();
        //todo: change to axis name
        if (Input.GetMouseButtonDown(0))
        {
            
            if (movementComponent.Speed > 0.1f)
            {
                AnimatorRef.SetLayerWeight(1, 1);
                AnimatorRef.SetTrigger(NormalAttackHasCode);

            }
            else
            {
                ChangeAttackAnimation(basicAttackAnim);
                UpdateMovementComponent(false);

            }
            StartCoroutine(WaitAttackAnim(basicAttackAnim));
        }

    }

    public void ActivateWeaponCollider()
    {
        {
        currentWeapon.Attack(out GameObject[] Targets);
            if (Targets.Length == 0)
            {
                Debug.Log("No target damaged");
            }

        }
    }
    public void DeActivateWeaponCollider()
    {

    }


    /*Combo system*/
    public void DoDamage(float Damage)
    {
        lifeController.ModifyValue(Damage * -1);
    }

    void Death()
    {
        if (isDeath) return;
        AnimatorRef.SetTrigger("Death");
        UpdateMovementComponent(false);
        isDeath = true;
    }
    protected virtual void OnComboExecute(Combo current)
    {
        Debug.Log("Called current combo on character!: " + current.ToString());

        //reseteamos el movimiento para que cuando vuelva este en la animacion de idle 
        UpdateMovementComponent(false);
    }

    protected virtual void OnComoboStop(Combo current)
    {
        UpdateMovementComponent(true);
    }

    //Helpers

    public void ChangeAttackAnimation(AnimationClip anim)
    {
        animatorOverrideController["attackbasic"] = anim;

        AnimatorRef.SetTrigger("AttackBasic");
        Debug.Log("Change animation to: " + anim);
    }


    void UpdateMovementComponent(bool isActive)
    {
        if (isActive)
        {
            movementComponent.enabled = true;
            movementComponent.ResetAnim();
            movementComponent.SetMovementMode(EMoveState.Walking);
        }
        else
        {

            movementComponent.enabled = false;
            movementComponent.ResetAnim();
            movementComponent.SetMovementMode(EMoveState.Idle);
        }
    }

    // Update is called once per frame

    IEnumerator WaitAttackAnim(AnimationClip anim, float offsetTime = 0.1f)
    {
        //  UpdateMovementComponent(false);
        isAttacking = true;
        yield return new WaitForSeconds(anim.length + offsetTime);
        isAttacking = false;
        AnimatorRef.SetLayerWeight(1, 0);
        UpdateMovementComponent(true);
    }
}
