﻿/////////////////////////////////////////////////////////////////////////////////
////
////              Author: Guillermo Blanca
////              
////
////////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComboManager : MonoBehaviour
{
    public Combo[] comboList;
    /// <summary>
    /// Lista de los inputs que se deben estar comprobando
    /// </summary>
    public KeyCode[] input;
    /// <summary>
    /// Limite del buffer, cuando llegue al limite comprueba si hay combo y lo ejecuta, luego elimina la lista
    /// </summary>
    public int bufferLimit = 3;
    /// <summary>
    /// Tiempo limite que tiene el combo antes de que se borre la lista
    /// </summary>
    public float comboTimeLimit = 2;

    /// <summary>
    /// Devuelve la ultima vez que se encontro combo
    /// </summary>
    private float _lastComboTime;

    /// <summary>
    /// Define si se debe seguir actualizando o no 
    /// </summary>
    /// 
    private bool _update = true;
    private float _timer;
    /// <summary>
    /// Creamos una cola para ir incluyendo las keys que se van metiendo 
    /// </summary>
    private List<KeyCode> _buffer;
    /// <summary>
    /// El combo que se esta ejecutando, es nulo cuando no se esta ejecutando ningun combo
    /// </summary>
    private Combo _currentCombo;
    /// <summary>
    /// El combo anterior que se ejecuto
    /// </summary>
    private Combo _lastCombo;

    [HideInInspector()]
    public Character Owner;


    public bool debugMode = true;
    //Control methods

    /// <summary>
    /// Cambia el estado del combo manager 
    /// </summary>
    /// 
    public delegate void OnCurrentCombo(Combo currentCombo);
    public event OnCurrentCombo OnExecuteCombo;
    public event OnCurrentCombo OnEndCombo;
    public void ToggleComboCheck()
    {
        _update = !_update;
        this.enabled = _update;
    }
    /// <summary>
    /// Vuelve a detectar las comprobaciones de combo
    /// </summary>
    public void EnableComboCheck()
    {
        _update = true;
        enabled = _update;
    }
    public void DisableComboCheck()
    {
        _update = false;
        enabled = _update;
    }

    public void Start()
    {
        _buffer = new List<KeyCode>(bufferLimit);
    }
    public void Update()
    {
        //Comprobamos el input que se esta ejecutando 
        InputUdate();

        //actualizamos el combo hasta que termine su tiempo
        //dado que el tiempo quizas queramos que se vea limitado por el tiempo de una animacion podemos hacerlo en base a Animation.Lenght
        if (_currentCombo)
        {
            _timer += Time.deltaTime;
            _currentCombo.Execute(Owner);

            OnExecuteCombo?.Invoke(_currentCombo);

            if (_timer > _currentCombo.ComboTime)
            {
                _currentCombo.EndExecute();
                OnEndCombo?.Invoke(_currentCombo);

                SetCombo(null);
            }

        }



        if (_buffer.Count >= bufferLimit)
        {

            //mientras se ejecute un combo no permitimos nuevos combos ni registrar nuevos
            if (_currentCombo)
            {
                _buffer.Clear();
                return;
            }

            //sort combolist en vez de esto, es un parche
            foreach (var item in comboList)
            {
                if (item.HasCombo(_buffer.ToArray()))
                {
                    SetCombo(item);
                    _buffer.Clear();
                    return;
                }
            }
            _buffer.Clear();
        }


        //reseteamos el buffer cuando superamos el limite de tiempo siempre que se haya pulsado una tecla, el timer se resetea cuando se encuentra combo
        if (_buffer.Count > 0)
        {
            _lastComboTime += Time.deltaTime;
            if (_lastComboTime > comboTimeLimit)
            {
                _buffer.Clear();
                _lastComboTime = 0;
            }
        }
    }

    public void InputUdate()
    {
        for (int i = 0; i < input.Length; i++)
        {
            if (Input.GetKeyDown(input[i]))
            {
                _buffer.Add(input[i]);
            }
        }
    }
    private void SetCombo(Combo combo)
    {
        if (_currentCombo)
            _lastCombo = _currentCombo;
        _lastComboTime = 0;

        _currentCombo = combo;
        if (_currentCombo)
        {
            _currentCombo.OnStartCombo(Owner);
        }
        _timer = 0;
    }
    private void OnGUI()
    {
        if (!debugMode) return;
        const float height = 40;
        Rect rect = new Rect(0, 0, 200, height);
        Rect rectRight = new Rect(200, 0, 400, height);

        GUI.TextField(rect, "Input buffer");
        rect.y += height;
        for (int i = 0; i < _buffer.Count; i++)
        {
            rect.y += height;
            GUI.TextField(rect, string.Format("Key[{0}]: {1}", i, _buffer[i].ToString()));
        }

        rect.y += height;
        GUI.TextField(rect, _currentCombo == null ? "Null combo" : "current: " + _currentCombo.ToString());
        rect.y += height;
        GUI.TextField(rect, _lastCombo == null ? "Null combo" : "last: " + _lastCombo.ToString());

        for (int i = 0; i < comboList.Length; i++)
        {
            string combination = comboList[i].ToString();
            for (int x = 0; x < comboList[i].Combination.Length; x++)
            {
                combination = combination.Insert(combination.Length, " " + comboList[i].Combination[x].ToString());
            }

            GUI.TextField(rectRight, combination);
            rectRight.y += height;
        }
    }
}