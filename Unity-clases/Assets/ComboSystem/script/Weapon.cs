﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    public GameObject Owner;
    public float Damage = 10;
    public float damageRadius = 0.35f;
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnDrawGizmos()
    {
        if(Physics.SphereCast(transform.position, damageRadius,transform.forward,out RaycastHit hit))
        {
            LifeController life = hit.collider.GetComponent<LifeController>();
            
            Gizmos.color = life && hit.collider.gameObject != Owner ? Color.green : Color.red;
        }


        Gizmos.DrawWireSphere(transform.position, damageRadius);
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("Collision from " + transform.parent.parent.name + " to: " + other.gameObject.name);
        if (other.gameObject != Owner)
        {
            LifeController controller = other.gameObject.GetComponent<LifeController>();
            if (controller)
            {
                controller.ModifyValue(Damage * -1);
            }
        }
    }

    public void Attack(out GameObject[] Targets)
    {
        var raycasts =Physics.SphereCastAll(transform.position, damageRadius,transform.forward);
        List<GameObject> targets = new List<GameObject>();
        foreach (var item in raycasts)
        {
            if (item.collider.gameObject == Owner)
                continue;

            var controller = item.collider.gameObject.GetComponent<LifeController>();
            if(controller)
            {
                targets.Add(controller.gameObject);
                controller.ModifyValue(-Damage);
            }
        }

        Targets = targets.ToArray();
    }
}
