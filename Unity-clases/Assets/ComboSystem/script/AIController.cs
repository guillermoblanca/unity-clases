﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
[RequireComponent(typeof(NavMeshAgent))]
public class AIController : MonoBehaviour
{
    public enum EAIState
    {
        Idle,
        Chase,
        FindingPlayer,
        Patrol,
        Attacking

    }

    public EAIState CurrentState = EAIState.Idle;
    protected NavMeshAgent navMeshAgent;
    protected Animator animator;
    protected LifeController lifeController;
    [Header("AI Sense")]
    public float detectionRange = 3;
    public float attackRange = 1;
    public float attackRate;
    public float attackTimer = 0;
    public float attackDamage = 10;
    public LayerMask detectMask;
    public int SpeedHasCode = Animator.StringToHash("Speed");

    public Weapon currentWeapon;
    public AnimationClip NormalAttackAnim;
    public AnimationClip DefensifeAttackAnim;
    public AnimationClip DeathAnim;
    // Start is called before the first frame update
    void Start()
    {
        navMeshAgent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        lifeController = GetComponent<LifeController>();
        lifeController.OnDeath.AddListener(Death);
        currentWeapon.Damage = attackDamage;
    }

    // Update is called once per frame
    void Update()
    {
        var raycastHits = Physics.SphereCastAll(transform.position, detectionRange, transform.forward, detectMask);

        foreach (var ray in raycastHits)
        {
            var Character = ray.collider.GetComponent<Character>();

            if (Character)
            {
                bool inAttackRange = (Character.transform.position - transform.position).sqrMagnitude < attackRange;

                if (inAttackRange) //si esta en el rango de ataque
                {
                    if (CurrentState != EAIState.Attacking) //si es la primera vez que entra al ataque
                    {
                        CurrentState = EAIState.Attacking;
                        attackTimer = attackRate; //hacemos que ataque si o si 

                    }


                    if (attackTimer > attackRate) //reiniciamos cada vez que se ataque
                    {
                        //animacion de ataque
                        animator.SetTrigger("AttackBasic");
                        attackTimer = 0;
                        navMeshAgent.isStopped = true;
                        transform.LookAt(Character.transform);
                    }

                }
                else //no esta en rango de ataque por lo que se encuentra buscando al jugador mientras este dentro del rango
                {
                    navMeshAgent.isStopped = false;
                    navMeshAgent.SetDestination(Character.transform.position);
                    CurrentState = EAIState.Chase;

                }
                break;
            }

        }

        if (raycastHits.Length == 0) //en caso de no encontrar a nadie entra en estado idle
        {
            CurrentState = EAIState.Idle;
        }

        if (navMeshAgent.remainingDistance > navMeshAgent.stoppingDistance)
        {
            //todo: rework this
            animator.SetFloat(SpeedHasCode, 1);
        }
        else
        {
            animator.SetFloat(SpeedHasCode, navMeshAgent.remainingDistance - navMeshAgent.stoppingDistance);
        }


        if (CurrentState == EAIState.Attacking) //actualizamos el timer de ataque en aso de estar en modo ataque
        {
            attackTimer += Time.deltaTime;
        }
    }
    public void ActivateWeaponCollider()
    {
        {
            currentWeapon.Attack(out GameObject[] Targets);

            if(Targets.Length ==0) //no se ha encontrado enemigo
            {
                //find new target
                Debug.Log("There is no target damaged");
            }
        }

    }
    public void DeActivateWeaponCollider()
    {
    }
    private void Death()
    {
        animator.SetTrigger("Death");
        navMeshAgent.isStopped = true;
        navMeshAgent.enabled = false;
        enabled = false;
    }
    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, detectionRange);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, attackRange);
    }
}
