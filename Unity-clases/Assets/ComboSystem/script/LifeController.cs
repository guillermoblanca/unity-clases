﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class FloatEvent : UnityEvent<float> { }
public class LifeController : MonoBehaviour
{
    [SerializeField] protected float currentHealth =0;
    [SerializeField] protected float maxHealth=100;
    public FloatEvent OnHealthChanged;
    public UnityEvent OnDeath;
    private void Start()
    {
        currentHealth = maxHealth;
    }

    public void ModifyValue(float addValue)
    {
        currentHealth = Mathf.Clamp(currentHealth + addValue, 0, maxHealth);

        OnHealthChanged?.Invoke(currentHealth);
        
        if(currentHealth <= 0)
        {
            Death();
        }
    }

    protected virtual void Death()
    {
        OnDeath?.Invoke();
    }
}
