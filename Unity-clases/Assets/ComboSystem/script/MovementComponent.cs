﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EMoveState
{
    Idle,
    Walking,
    Running
}

[RequireComponent(typeof(CharacterController))]
public class MovementComponent : MonoBehaviour
{

    public Vector3 desiredMoveDirection;
    public bool blockRotationPlayer;
    public float desiredRotationSpeed = 0.1f;

    Vector2 input;
    Animator animator;
    Camera cam;
    CharacterController controller;

    [HideInInspector] public float Speed;
    public float WalkSpeed = 3.0f;
    public float RunSpeed = 6.0f;
    private float Velocity = 3.0f;
    public float allowPlayerRotation = 0.1f;
    public bool isGrounded;

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float HorizontalAnimSmoothTime = 0.2f;
    [Range(0, 1f)]
    public float VerticalAnimTime = 0.2f;
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.15f;


    public int SpeedHasCode = Animator.StringToHash("Speed");
//    public int InputXHasCode = Animator.StringToHash("InputX");
//    public int InputZHasCode = Animator.StringToHash("InputZ");
    private float verticalVel;
    private bool isRunning;
    private Vector3 moveVector;
    public EMoveState CurrentState { get; private set; }
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        cam = Camera.main;
        controller = GetComponent<CharacterController>();
        SetMovementMode(EMoveState.Running);
    }
    private void Update()
    {
        InputMagnitude();

        //If you don't need the character grounded then get rid of this part.
        isGrounded = controller.isGrounded;
        if (isGrounded)
        {
            verticalVel -= 0;
        }
        else
        {
            verticalVel -= 2;
        }
        moveVector = new Vector3(0, verticalVel, 0);
        controller.Move(moveVector);


        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            isRunning = true;
            SetMovementMode(EMoveState.Running);
        }
        else if (Input.GetKeyUp(KeyCode.LeftShift))
        {
            isRunning = false;
            SetMovementMode(EMoveState.Walking);
        }

    }
    // Update is called once per frame
    public void SetMovementMode(EMoveState state)
    {
        CurrentState = state;
        switch (state)
        {
            case EMoveState.Idle:
                Velocity = 0;
                break;
            case EMoveState.Walking:
                Velocity = WalkSpeed;
                break;
            case EMoveState.Running:
                Velocity = RunSpeed;
                break;
            default:
                break;
        }
    }
    public void PlayerMoveAndRotation(Vector2 direction)
    {
        if (CurrentState == EMoveState.Idle) return;

        var forward = cam.transform.forward;
        var right = cam.transform.right;

        forward.y = 0;
        right.y = 0;

        forward.Normalize();
        right.Normalize();

        desiredMoveDirection = forward * direction.y + right * direction.x;
        if (!blockRotationPlayer)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
            controller.Move(desiredMoveDirection * Time.deltaTime * Velocity);

        }
    }

    public void RotateToCamera(Transform t)
    {
        var forward = cam.transform.forward;
        var right = cam.transform.right;

        desiredMoveDirection = forward;
        t.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), desiredRotationSpeed);
    }

    public void ResetAnim()
    {
        animator.SetFloat(SpeedHasCode, 0);
//      animator.SetFloat(InputXHasCode, 0);
//      animator.SetFloat(InputZHasCode, 0);

    }
    public void InputMagnitude()
    {
        input.x = Input.GetAxis("Horizontal");
        input.y = Input.GetAxis("Vertical");

        Speed = input.sqrMagnitude;

        SetMovementMode(Speed > 0 && isRunning ? EMoveState.Running : Speed > 0 ? EMoveState.Walking : EMoveState.Idle);


        if (CurrentState == EMoveState.Running)
        {
            Speed += 1;
        }


        animator.SetFloat(SpeedHasCode, Speed);
        //animator.SetFloat(InputXHasCode, input.x);
        //animator.SetFloat(InputZHasCode, input.y);

        if (Speed > allowPlayerRotation)
        {
            PlayerMoveAndRotation(input);

        }
        else if (Speed < allowPlayerRotation)
        {

        }
    }
}
