﻿/////////////////////////////////////////////////////////////////////////////////
////
////              Author: Guillermo Blanca
////              
////
////////////////////////////////////////////////////////////////////////////////
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Clase basica para ejecutar combos en un personaje
 * 
 * Cuando se encuentra la combinacion marcada se ejecuta la animacion correspondiente que se haya incorporado. 
 * En caso de no meter una animación saltara error.
 * 
 * El tiempo en el que permanece el combo lo determina la duración de la animación. 
 * 
 */

[System.Serializable, CreateAssetMenu(menuName = "Combo System/Combo")]
public class Combo : ScriptableObject
{
    public delegate void OnCombo();

    public event OnCombo OnComboStart;
    public event OnCombo OnComboExecute;
    public event OnCombo OnComboEnd;


    /// <summary>
    /// Combinaciones para ejecutar el combo
    /// </summary>
    public KeyCode[] Combination;
    /// <summary>
    /// Tiempo que tarda en ejecutarse  el combo
    /// </summary>
    public float ComboTime { get { return animation == null ? 2: animation.length; } }


    //La animacion de la que sacamos el tiempo
    public AnimationClip animation;
    /// <summary>
    /// Valor que define el daño que hace el arma en caso de chocar con alguien que pueda hacer daño
    /// </summary>
    public int Damage = 200;
    /// <summary>
    /// Constructor
    /// </summary>
    /// 
    public Combo()
    {
        Combination = new KeyCode[3];
    }


    /// <summary>
    /// Se llama mientras el combo se este ejecutando. Para parar al combo se debe 
    /// </summary>
    /// <param name="deltaTime"></param>
    public void Execute(Character Owner)
    {
       
        Debug.Log("Execute combo " + ToString() + " owner " + Owner.ToString());
    }

    /*Se llama al inicio del combo*/
    public void OnStartCombo(Character Owner)
    {
        Owner.ChangeAttackAnimation(animation);
        Debug.Log("Combo started: " + ToString());
    }

    /// <summary>
    /// Devuelve si tiene combo en base al buffer que se meta
    /// </summary>
    /// <param name="Buffer"></param>
    /// <returns></returns>
    public bool HasCombo(KeyCode[] Buffer)
    {
        if (Buffer.Length == 0) return false;

        bool HasCombo = true;
        for (int i = 0; i < Buffer.Length; i++)
        {
            for (int x = 0; x < Combination.Length; x++)
            {
                HasCombo &= Combination[x] == Buffer[x];

                //la key no coincide 
                if (!HasCombo)
                    return false;
            }
        }

        return HasCombo;
    }

    //Called when the combo has to end
    public void EndExecute()
    {
        OnComboEnd?.Invoke();
    }
}
